require 'sinatra'

get '/' do
  erb :index
end

get '/work' do
  erb :work
end

not_found do
  erb :"404"
end
