var c = document.getElementById("c");
c.width = window.innerWidth;
c.height = window.innerHeight;
var ctx = c.getContext("2d");

var t = 0, w = c.width, h = c.height;

L = [{'x':w/2, 'y':h/2, 'r' : 5}];

function add()
{
  var x,y, rand;

  do {
    rand = (Math.random()-0.5) * 100;
    x = L[L.length-1].x;
    y = L[L.length-1].y;
    if (Math.random() > 0.2)
      if (Math.random() > 0.5)
        x += rand;
      else
        x -= rand;
    if (Math.random() > 0.2)
      if (Math.random() > 0.5)
        y += rand;
      else
        y -= rand;
  } while (x <= 0 || x >= w || y <= 0 || y >= h);

  L.push({'x': x, 'y': y, 'r': Math.random()*3});

   if (L.length > 2)
     L.shift();
}

function lines()
{
  add();
  ctx.beginPath();
  for (var i = 0; i < L.length; i++)
  {
    ctx.arc(L[i].x, L[i].y, L[i].r, 0, 2*Math.PI);
  }
  ctx.fill();
  ctx.beginPath();
  for (var i = 0; i < L.length; i++)
  {
    ctx.lineTo(L[i].x, L[i].y);
  }
  ctx.stroke();

}

setInterval(lines, 100);
